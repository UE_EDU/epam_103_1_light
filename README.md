# EPAM_103_1_Lighting_Introduction

Developed with Unreal Engine 5

## Task Description: 

The purpose of this Lesson is to learn about different Types of Lights in the UE and how to utilize them. 

You will learn about Static and Dynamic lighting. How to Bake lighting in Unreal as well as how different light sources affect objects.  

 Please set Dynamic Global Illumination Method to None and Reflection Method to Screen Space in the Project Settings! Otherwise, you won’t be able to build the lighting since Software Raytracing (Lumen) is set as GI by default in UE5!
 
## List of maps that should be present in the Project’s Content Folder: 

1. The map that uses Post Processing Volumes with Lightmass Portal and baked lighting  
2. The map that has examples of Static, Stationary and Movable lights 

### Requirements for the maps: 

#### Post Processing Volume: 

1. The map has two rooms lit only by the Directional Light 
2. Directional Light is set to Stationary Mobility 
3. Lightmass Importance Volume covers both rooms 
4. There is one Post Processing Volume for each room 
5. Post Processing Volume in the lit room has Exposure Compensation enabled, and is set to Unbound 
6. Post Processing Volume in the dark room has different Exposure Compensation values from the first room 
7. Priority is set to 1.0 
8. There is a single Lightmass Portal actor with the default values that is pointing to both rooms 
9. Lighting in the Level is built using Medium quality 

Please note: Please use Basic Level Template and reconstruct the building from the Video as best you can with the Cube actors! 

#### Different Light types: 

1. The map has three sets of light actors. Each set is comprised of Point, Cone, and Rect Light actors that do not overlap 
2. Lightmass Importance Volume covers all buildings in the Level 
3. In the First set of light actors, all of them are set to Static Mobility 
4. Actors in the Second set are set to Stationary Mobility 
5. Actors in the Third set are set to Movable Mobility 
6. With each set of light actors, some Static meshes are present. Simple shapes (Cube, Cone, Cylinder, Sphere) from the Place Actors can be used 
7. Lighting in the Level is built using Medium quality 

Please note: Play around with the different settings of the Light Actors. As well as different Static Meshes. Just to explore the effect of the different light sources on the different shapes.